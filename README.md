# Podwrite

System for automated podcast publishing.

This is what I use to publish [GNU World Order](http://gnuworldorder.info) and [Chronicles & Commons](http://mixedsignals.ml/cnc).


## Requires

**Podwrite** requires:

* GNU bash
* GNU sed
* [html2text](https://github.com/Alir3z4/html2text)

**mkenc** requires:

* sox
* oggenc (if you want to encode to ogg)
* opusenc (if you want to encode to ogg)
* speexenc (if you want to encode to ogg)
* fdkaac (if you want to encode to ogg)

This has only been tested on open source operating systems, specifically on FreeBSD, NetBSD, and Linux.


## Install

Run the ``install.sh`` command.

## Uninstall

Run the ``uninstall.sh`` command for instructions.

## Docs

http://slackermedia.info/podwrite