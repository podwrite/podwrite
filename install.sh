#!/bin/sh
# GPLv3

VERSION=1.1
L=`arch | rev | cut -f1 -d"_" | rev`
if [ "x$L" != "x64" ]; then
    echo $L
    L=""
fi

EXEC=usr/local/bin
LIB=usr/local/lib"${L}"
DOC=usr/local/share/doc
MAN=usr/local/share/man
DESTDIR=${DESTDIR:-/}
LOG=podwrite.install.log
DEBUG=false

trail() {
    local VAR=`echo "${1}" | rev | cut -b1`
    if [ "${VAR}" != "/" ]; then 
	echo "${1}/"
    else
	echo "${1}"
    fi
}

while [ True ]; do
if [ "$1" = "--help" -o "$1" = "-h" ]; then
    echo "Generic installer for podwrite and mkenc."
    echo "$VERSION"
    echo "installer [options]"
    echo "--exec (default: usr/local/bin)"
    echo "--lib  (default: usr/local/lib${L}"
    echo "--doc  (default: usr/local/share/doc"
    echo "--man  (default: usr/local/share/man"
    echo "--dest (default: / )"
    exit
elif [ "$1" = "--exec" ]; then
    EXEC=`trail ${2}`
    shift 2
elif [ "$1" = "--lib" ]; then
    LIB=`trail ${2}`
    shift 2
elif [ "$1" = "--doc" -o "$1" = "--docs" ]; then
    DOC=`trail ${2}`
    shift 2
elif [ "$1" = "--man" ]; then
    MAN=`trail ${2}`
    shift 2
elif [ "$1" = "--log" ]; then
    LOG=$2
    shift 2
elif [ "$1" = "--dest" -o "$1" = "--destdir" ]; then
    DESTDIR=`trail ${2}`
    `${DEBUG}` && echo "${DESTDIR}"
    shift 2    
elif [ "$1" = "--verbose" ]; then
    DEBUG=true
    shift 1
else
    break
fi
done

`$DEBUG` && echo $DESTDIR
`$DEBUG` && echo $EXEC
`$DEBUG` && echo $DOC
`$DEBUG` && echo $MAN
`$DEBUG` && echo $LIB

## bin
mkdir -p "${DESTDIR}${EXEC}"
find bin/ -exec install -m 755 -o root -g root -v {} "${DESTDIR}${EXEC}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"
`${DEBUG}` && echo "Exec dir set to ${DESTDIR}${EXEC}"

sed -i "s_./include_usr/local/lib${L}_g" "${DESTDIR}${EXEC}"/podwrite
sed -i "s_./include_usr/local/lib${L}_g" "${DESTDIR}${EXEC}"/mkenc

## lib include
mkdir -p "${DESTDIR}${LIB}"
find include/ -exec install -m 755 -o root -g root -v {} "${DESTDIR}${LIB}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"
`${DEBUG}` && echo "Library/include set to ${DESTDIR}/${LIB}"

## conf
#mkdir -p "${DESTDIR}${CONF}"
#find . -type f -iname "dot-podwriterc" -exec install -m 770 -v {} "${DESTDIR}${CONF}/.podwriterc" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"

## for uninstaller
echo "${DESTDIR}${DOC}/podwrite" >> "${LOG}"
echo "${DESTDIR}${DOC}/podwrite/example" >> "${LOG}"

## docs
mkdir -p "${DESTDIR}${DOC}/podwrite/example"
DOC="${DOC}/podwrite/example"
`${DEBUG}` && echo "Documentation set to ${DOC}"

find . -type f -iname "dot-podwriterc" -exec install -m 755 -o root -g root -v {} "${DESTDIR}${DOC}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"

find example -exec install -m 755 -o root -g root -v {} "${DESTDIR}${DOC}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"

find . -type f -name "README.md" -exec install -m 755 -o root -g root -v {} "${DESTDIR}${DOC}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"

### man

mkdir -p "${DESTDIR}${MAN}/man1"
MAN="${MAN}/man1"

for i in doc/man/* ; do
    gzip $i
done

find doc/man -exec install -m 755 -o root -g root -v {} "${DESTDIR}${MAN}" \; 2>/dev/null | tee | awk -F"'" '{ print $4 }' >> "${LOG}"

sed -i '/^\s*$/d' "${LOG}"

echo "Podwrite installed."
