#!/bin/sh

echo "To uninstall podwrite, you need an install log."
echo "If you don't have an install log, reinstall podwrite"
echo "just to generate the log."
echo " "
echo "Uninstall like this:"
echo " "
echo "trashy $(< podwrite.install.log)"
echo " "
echo "If you do not have trashy, you may use rm -i:"
echo " "
echo "rm -i -r $(< podwrite.install.log)"
echo " "
