.\" Podwrite Oggcast Poster
.TH "Podwrite" "1" ""  "Klaatu" ""
.SH "NAME"
Podwrite \- Generate feeds, HTML, and post shows.
.SH "SYNOPSIS"
\fBpodwrite\fP [ --options ] TITLE
.SH "DESCRIPTION"
.PP 
\fBPodwrite\fP is an application that helps you generate RSS and Atom
feeds and an HTML page, and then it  pushes those documents along with
your show media to your server. Aside from automation, features
include SHA256 signatures for the downloads that you are offering and
encrypted local storage of your FTP password using GnuPG.
.PP
\fBPodwrite\fP is the third and final step in a larger workflow. Before running
\fBpodwrite\fP, you may want to run \fBpodprep\fP and \fBpodstitch\fP.
.PP
.SH USE CASE AND WORKFLOW
There are two ways to use \fBPodwrite\fP: with options provided to a shell command, or with a config file. Shell options override config arguments.
.PP
.nr step 1 1
The intended workflow is:
.IP \n[step] 5
Produce your 'cast and export to the media types that you deliver,
such as Ogg Vorbis, Speex, and Webm. Your files names MUST be in the
format $TITLE_$EPISODE.$FORMAT
.IP \n+[step]
Place your media files in a dedicated directory. It should be a dedicated directory because \fBPodwrite\fP uploads the whole directory to the server, so if you have other files in it (or even old versions of previous shows) then they will ALL be uploaded, which could potentially be a royal waste of bandwidth and time.
.IP \n+[step]
Generate shownotes in standard HTML, and if you have related links then generate a document of links, also in standard HTML.
.IP \n+[step]
Run podwrite.
.IP \n+[step]
Drink coffee. You've earned it.

.SH OPTIONS

.TP
.B -i, --include
File containing your shownotes. To build your post, this is required.
.TP
.B -l, --linkdoc
File containing your links. To build your post, this is required.
.TP
.B -e, --episode
Episode number. To build your post, this is a required value.
.TP
.B -m, --media
Path to directory containing your media files. Again: your media files
must be named \fB$TITLE_$EPISODE.$FORMAT\fP to be valid. Maybe in some
future version this will be malleable but for now, this is hardcoded.
.TP
.B -f, --format
The formats you want to upload. These can be any format,
\fBpodwrite\fP doesn't do any kind of validation or anything, but they
should be the same as the file extensions you use on your media
files.
.PP In other words, if you have gnuWorldOrder_8x01.ogg in your media directory, then specify 'ogg' as your format. If you have gnuWorldOrder_8x01.spx, then use 'spx'. If you have both (comma-delimited, NO SPACES). For example:
.IP
--format ogg,spx
.TP
.B -A, --author
Name of the author, required by the RSS and Atom feeds in order to be
valid feeds.
.TP
.B -E, --email
Email of the author, required by the RSS and Atom feeds in order to
be valid feeds.

.SH SERVER OPTIONS
.TP
.B -w, --webserver
.br
Domain name of the server you will be uploading to.
.TP
.B -p, --password
Password to login to server.
.TP
.B -u, --user
User to login to server.

.SH PROCESSING FLAGS
.TP
.B -c, --config
Location of the (optional) config file. The default is
$HOME/.config/podwriterc but you are welcome to change it in the code,
or just define it with this option as needed.
.TP
.B -p, --publish
Yes, publish to the server. The default behaviour of \fBpodwrite\fP is
to NOT publish. It only builds; you should look at the files, make
sure everything is OK, and then run \fBpodwrite\fP with the --no-build
flag combined with the --publish flag, at least until you build up
confidence in \fBpodwrite\fP (if ever you do; heck, I wrote it and I
still build first and publish second).
.TP
.B -n, --no-build
Do not actually change any files. This can be used as either a
dry-run, just to make sure you actually have all the files you need
for a successful build, or as a first-pass which you can then follow
with the --publish option to push the changes to your server.
.TP
.B -D, --delete
Delete the old versions of the HTML and XML files from the work
directory. This will not delete the media files.
.PP
Frankly, you probably shouldn't use this option at all. It's too
dangerous. Manage your own files.
.TP
.B -h, --help
Show a brief help message and exit.
.PP

.SH CONFIGURATION FILE
.PP
Let's face it, if you're posting oggcast episodes on a regular basis,
you probably are too busy to type out every single option and flag
that \fBpodwrite\fP requires from you. For this reason, \fBpodwrite\fP
looks to $HOME/.config/podwriterc but you are welcome to change the
location in the code or with the -c option.
.PP
A configuration file may define any option you can pass on the command
line, although certain values don't make sense to define in a config
file (such as the episode number).
.PP
The format of the config file is:
.IP
[showTitle]
key = value
.TP
For values that span shows, use a section with 'global' as its title.
.IP
[global]
key = value
.TP
For example:
.IP
[global]
author = Klaatu
email = klaatu@example.com
.PP
[gnuWorldOrder]
include = shownotes.htm
linkdoc = links.htm
webserver = gnuworldorder.info
media = audiophile
.PP
.SH CONFIG FILE KEYS
.nf
.IP epnum = episode number. Why you would ever define this in a config file, I have no idea.
.IP include = path to your shownotes file. I call mine shownotes.htm (notice the .htm extension; this is so that when I remove my working files, I can safely do `trash *ml` without clearing out my shownotes).
.IP linkdoc = path to the file containing the link section of your shownotes.
.IP server = domain name and tld of your server; for instance, example.com or gnuworldorder.info
.IP media = path to your media directory.
.IP formats = comma-delimited list of formats you use. No Spaces; for example: ogg,spx,webm
.IP delete = clean up all the temporary files after finished? Set to 1 to delete files. This deletes files the HTML and XML files that podwrite has downloaded, modified, and then uploaded; use with caution.
.IP no_build = do not download and modify HTML or XML files; basically a dry-run. Can be used in conjunction with the publish option to use podwrite in upload mode only. Set to 1 to activate no_build mode.
.IP skip_html = download and modify XML files only, skip the HTML creation. You might use this if your webpage structure doesn't happen to match with podwrite's pre-sets.
.IP pass = path to your password and username for your GnuPG-encrypted FTP or SSH login.
.IP author = name of the show author. Used in the feeds.
.IP email = name of the show email. Used in the feeds.
.fi
.PP
.SH SERVER CREDENTIALS
For improved security, the preferred way for \fBpodwrite\fP to read
your server username and password is from a GnuPG-encrypted file. If
you do not already have a GPG key on your system, create one, and then
use it to encrypt a plain text file containing the delimiter
character, your username, the delimiter, and your password. For
example:
.IP
,klaatu,mySuperSecret@@Pass12$wo9rd
.PP
As long as your delimiter is NOT contained in either your username or
your password, \fBpodwrite\fP will be able to parse the file and use
the values to login to your server.
.PP
This is the best way to enter your username and password, since if you
enter them in the shell, they persist in your history file. Using a
config file to point to an encrypted file at least keeps the values as
safe as GPG itself.
.PP
.SH "SEE ALSO"
.nf
.I podprep (1)
.I podstitch (1)
.I sftp (1)
.I lftp (1)
.I gpg (1)
.URL http://gnuworldorder.info/podwrite
.URL http://gitlab.com/podwrite
.fi

.PP
.SH "AUTHORS"
.nf
Klaatu (klaatu@member.fsf.org)
.fi
.PP
.SH "BUGS"
Report via email or on gitlab.com. Also feel free to fix them and
request a merge.
.fi
